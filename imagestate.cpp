#include "imagestate.h"

#include <QPointer>
#include <QPainter>
#include <QTransform>
#include <QFileDialog>
#include <QPushButton>

#include "imageview.h"
#include "selectstate.h"

class ImageStatePrivate
{
public:
    ImageStatePrivate()
    {}

    ~ImageStatePrivate()
    {}

    QImage m_imgBuffer;

    QPointer <ImageView> m_view;

    QTransform m_transform;

}; /// class ImageStatePrivate

ImageState::ImageState()
    : QObject(),
      d_ptr(new ImageStatePrivate)
{
}

ImageState::~ImageState()
{
}

void ImageState::paint(QPainter *_painter)
{
    Q_ASSERT(_painter);

    Q_D(ImageState);

    Q_ASSERT(d->m_view);

    if ( d->m_imgBuffer.isNull() )
        updateBuffer();

    _painter->save();

    _painter->fillRect(d->m_view->rect(), Qt::black);

    if ( false == d->m_imgBuffer.isNull() )
    {
        QRect p_rectCenter = d->m_imgBuffer.rect();
        p_rectCenter.moveCenter(d->m_view->rect().center());

        _painter->drawImage(p_rectCenter, d->m_imgBuffer);
    }

    _painter->restore();
}

ImageView *ImageState::view() const
{
    Q_D(const ImageState);

    return d->m_view;
}

QRect ImageState::mapToScreen(const QRect &_rect) const
{
    Q_D(const ImageState);

    if ( d->m_imgBuffer.isNull() )
        return QRect();

    return d->m_transform.inverted().mapRect(_rect);
}

QPoint ImageState::mapToScreen(const QPoint &_pos) const
{
    Q_D(const ImageState);

    if ( d->m_imgBuffer.isNull() )
        return QPoint();

    return d->m_transform.inverted().map(_pos);
}

QPoint ImageState::mapToImage(const QPoint &_pos) const
{
    Q_D(const ImageState);

    if ( d->m_imgBuffer.isNull() )
        return QPoint();

    return d->m_transform.map(_pos);
}

QRect ImageState::mapToImage(const QRect &_rect) const
{
    Q_D(const ImageState);

    if ( d->m_imgBuffer.isNull() )
        return QRect();

    return d->m_transform.mapRect(_rect);
}

void ImageState::setView(ImageView *_view)
{
    Q_ASSERT(_view);

    Q_D(ImageState);

    d->m_view = _view;

    connect(_view, SIGNAL(sgnSizeChanged(QSize)),
            this, SLOT(cleanBuffer()));
}

void ImageState::cleanBuffer()
{
    Q_D(ImageState);

    d->m_imgBuffer = QImage();
}

void ImageState::updateBuffer()
{
    Q_D(ImageState);

    Q_ASSERT(d->m_view);

    cleanBuffer();

    if ( d->m_view->image().isNull() )
        return;

    const double p_fRatioImg = 1.0 * d->m_view->image().width() /
                               d->m_view->image().height();
    const double p_fRatioScr = 1.0 * d->m_view->width() /
                               d->m_view->height();

    if ( p_fRatioImg >= p_fRatioScr )
        d->m_imgBuffer = d->m_view->image().scaledToWidth(d->m_view->width());
    else
        d->m_imgBuffer = d->m_view->image().scaledToHeight(d->m_view->height());

    QRect p_rectScene = d->m_imgBuffer.rect();
    p_rectScene.moveCenter(d->m_view->rect().center());

    QTransform::quadToQuad( QPolygon(p_rectScene),
                            QPolygon(d->m_view->image().rect()), d->m_transform );
}

