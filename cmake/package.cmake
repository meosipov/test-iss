# Cpack macro
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Test task for ISS")
set(CPACK_PACKAGE_VENDOR "Michael Osipov")
set(CPACK_PACKAGE_CONTACT "osmiev@gmail.com")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "osmiev@gmail.com")

# Autogenerating dependencies information
set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
set(CPACK_RPM_PACKAGE_AUTOREQ ON)

include(CPack)
