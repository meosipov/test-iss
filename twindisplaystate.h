#pragma once

#include "imagestate.h"

#include <QImage>

class TwinDisplayState : public ImageState
{
    Q_OBJECT

public:
    TwinDisplayState();
    ~TwinDisplayState();

protected slots:
    void cleanBuffer() override;
    void updateBuffer();

protected:
    void paint(QPainter *_painter) override;
    void mouseMove(const QPoint &) override;

    bool m_saved;

    QPoint m_posMouse;

    QImage m_imgBufferFiltered;

}; /// class TwinDisplayState
