#include "twindisplaystate.h"

#include <QPainter>

#include "imageview.h"

#include <QDebug>

TwinDisplayState::TwinDisplayState()
    : ImageState(),
      m_saved(false)
{
}

TwinDisplayState::~TwinDisplayState()
{
}

void TwinDisplayState::cleanBuffer()
{
    ImageState::cleanBuffer();

    qDebug() << "Cleaning buffer";
    m_imgBufferFiltered = QImage();
}

void TwinDisplayState::updateBuffer()
{
    const QRect p_rectSelect = mapToScreen( view()->selection() );
    m_imgBufferFiltered = view()->filtered().scaled(p_rectSelect.size());

    qDebug() << "Size: " << m_imgBufferFiltered.size();
}

void TwinDisplayState::paint(QPainter *_painter)
{
    ImageState::paint(_painter);

    _painter->save();

    if ( m_imgBufferFiltered.isNull() )
        updateBuffer();

    _painter->setPen(QPen(Qt::white, 2));

    QRect p_rectClip(0, 0, m_posMouse.x(), view()->height());
    _painter->setClipRect(p_rectClip);

    const QRect p_rectScreen = mapToScreen(view()->selection());
    _painter->drawImage(p_rectScreen.topLeft(), m_imgBufferFiltered);

    _painter->setClipping(false);

    _painter->drawRect(mapToScreen(view()->selection()));

    _painter->drawLine(QPoint(m_posMouse.x(), 0),
                       QPoint(m_posMouse.x(), view()->height()));

    _painter->restore();
}

void TwinDisplayState::mouseMove(const QPoint &_pos)
{
    m_posMouse = _pos;

    view()->update();
}
