#include "selectstate.h"

#include <QVector2D>
#include <QPainter>
#include <QAction>
#include <QDebug>

#include "imageview.h"
#include "processstate.h"

SelectState::SelectState()
    : ImageState(),
      m_sizeCore(0)
{
}

SelectState::~SelectState()
{}

void SelectState::paint(QPainter *_painter)
{
    Q_ASSERT(_painter);

    ImageState::paint(_painter);

    _painter->save();

    _painter->setRenderHint(QPainter::Antialiasing);

    if ( false == m_rectSelection.isNull() )
    {
        const QRect p_rectScreen = mapToScreen(m_rectSelection);

        QRegion p_region(view()->rect());
        p_region = p_region.subtracted(QRegion(p_rectScreen));

        _painter->setClipRegion(p_region);
        _painter->setOpacity(0.8);
        _painter->fillRect(view()->rect(), Qt::black);

        _painter->setOpacity(1.0);
        _painter->setClipping(false);

        _painter->setPen( QPen(Qt::white, 2) );
        _painter->drawRect(p_rectScreen);
    }

    _painter->restore();
}

void SelectState::mouseMove(const QPoint &_pos)
{
    if ( false == m_pressState )
        return;

    m_line.setP2(bordered(_pos));
    m_rectSelection = QRect(m_line.p1(), m_line.p2()).normalized();
    m_rectSelection = mapToImage(m_rectSelection);

    view()->update();
}

void SelectState::mousePress(const QPoint &_pos)
{
    m_line.setP1(bordered(_pos));

    m_pressState = true;
}

void SelectState::mouseRelease(const QPoint &_pos)
{
    m_pressState = false;

    if ( QVector2D(_pos - m_line.p1()).length() < 3 )
        m_rectSelection = QRect();

    view()->setSelection(m_rectSelection);
    view()->update();
}

QPoint SelectState::bordered(const QPoint &_pos) const
{
    QPoint p_pos = mapToImage(_pos);

    p_pos.setX( qMax(0, p_pos.x()) );
    p_pos.setX( qMin(p_pos.x(), view()->image().rect().width()) );

    p_pos.setY( qMax(0, p_pos.y()) );
    p_pos.setY( qMin(p_pos.y(), view()->image().rect().height()) );

    return mapToScreen(p_pos);
}

// cppcheck-suppress unusedFunction
void SelectState::setValue(double _val)
{
    m_sizeCore = _val;
}
