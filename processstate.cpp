#include "processstate.h"

#include <QPainter>
#include <QDebug>

#include <qmath.h>

#include "imageview.h"
#include "twindisplaystate.h"

#include <opencv2/opencv.hpp>

ProcessState::ProcessState(const int _size)
    : ImageState(),
      m_sizeCore(_size),
      m_angle(.0)
{
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
    m_timer.start(50);

    connect(&m_thread, SIGNAL(started()),
            this, SLOT(process()), Qt::DirectConnection);
    connect(&m_thread, SIGNAL(finished()),
            this, SLOT(done()), Qt::QueuedConnection);
    m_thread.start();
}

ProcessState::~ProcessState()
{
    m_thread.wait();
    m_timer.stop();
}

void ProcessState::paint(QPainter *_painter)
{
    ImageState::paint(_painter);

    _painter->save();

    _painter->setRenderHint(QPainter::Antialiasing);

    _painter->setOpacity(0.5);
    _painter->fillRect(view()->rect(), Qt::black);

    _painter->setOpacity(1.0);

    _painter->setPen(QPen(Qt::white, 2));

    QRect p_rectProgress(0, 0, 50, 50);
    p_rectProgress.moveCenter(QPoint(0, 0));

    _painter->translate(view()->rect().center());
    _painter->rotate(m_angle);
    _painter->drawArc(p_rectProgress, 0 * 16, 275 * 16);
    _painter->restore();
}

void ProcessState::nextFrame()
{
    m_angle += 4;

    if (m_angle > 360)
        m_angle -= 360;

    if (view())
        view()->update();
}

void ProcessState::process()
{
    qDebug() << "Processing";
    const QImage p_image = view()->image().copy(view()->selection());

    cv::Mat p_cvSrc = toCvImage(p_image);
    cv::Mat p_cvRes;

    qDebug() << "converting to cv: " << p_image.size()
             << " -> " << p_cvSrc.cols << "x" << p_cvSrc.rows;

    cv::medianBlur(p_cvSrc, p_cvRes, m_sizeCore);

    qDebug() << "Filtering: " << p_cvRes.cols << "x" << p_cvRes.rows;

    m_image = toQImage(p_cvRes);

    qDebug() << "Result image: " << m_image.size();

    m_thread.exit();
}

QImage ProcessState::toQImage(const cv::Mat &_mat)
{
    return QImage((const uchar *) _mat.data, _mat.cols,
                  _mat.rows, _mat.step, QImage::Format_RGB888).rgbSwapped();
}

cv::Mat ProcessState::toCvImage(const QImage &_image)
{
    QImage p_img = _image;
    if ( _image.format() != QImage::Format_RGB888 )
        p_img = p_img.convertToFormat(QImage::Format_RGB888);

    p_img = p_img.rgbSwapped();

    cv::Mat p_mat(p_img.height(), p_img.width(), CV_8UC3,
                  p_img.bits(), p_img.bytesPerLine());

    return p_mat.clone();
}

void ProcessState::done()
{
    view()->setFiltered(m_image);
    view()->setState(new TwinDisplayState);
}

