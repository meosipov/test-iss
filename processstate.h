#pragma once

#include "imagestate.h"

#include <QThread>
#include <QImage>
#include <QTimer>

namespace cv
{
class Mat;
}

class ProcessState final : public ImageState
{
    Q_OBJECT

public:
    explicit ProcessState(const int _size);
    ~ProcessState();

    void paint(QPainter *_painter) override;

private slots:
    void nextFrame();
    void process();
    void done();

private:
    static QImage toQImage(const cv::Mat &_mat);
    static cv::Mat toCvImage(const QImage &_image);

private:
    QTimer m_timer;
    QThread m_thread;
    int m_sizeCore;

    QImage m_image;

    double m_angle;
}; /// class ProcessState
