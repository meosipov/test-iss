﻿#pragma once

#include "imagestate.h"

#include <QRect>
#include <QLine>
#include <QPointer>
#include <QPushButton>
#include <QDoubleSpinBox>

class SelectState final : public ImageState
{
    Q_OBJECT

public:
    SelectState();
    ~SelectState();

    void paint(QPainter *_painter) override;

protected:
    void mouseMove(const QPoint &) override;
    void mousePress(const QPoint &) override;
    void mouseRelease(const QPoint &) override;

private:
    QPoint bordered(const QPoint &_pos) const;

private slots:
    void setValue(double _val);

private:
    QLine m_line;
    QRect m_rectSelection;

    double m_sizeCore = .0;

    bool m_pressState = false;

}; /// class SelectState
