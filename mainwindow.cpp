#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QAction>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QToolButton>

#include "selectstate.h"
#include "processstate.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_view = new ImageView(this);
    m_view->setState(new SelectState);

    setCentralWidget( m_view );

    initActions();
}

MainWindow::~MainWindow()
{
    if ( m_view->needSave() )
        save();

    delete ui;
}

void MainWindow::open()
{
    QFileDialog p_dlgOpen(nullptr, tr("Open Image"), "./",
                          tr("Images (*.png *.jpg *.bmp)"));

    QStringList p_listFiles;

    if ( p_dlgOpen.exec() )
        p_listFiles = p_dlgOpen.selectedFiles();

    if ( false == p_listFiles.isEmpty() )
    {
        QImage p_imgOpened = QImage(p_listFiles.first());
        m_view->setImage(p_imgOpened);
        m_view->setState(new SelectState);

        m_view->setSavedState(false);
        m_view->setSelection(QRect());
        m_view->setFiltered(QImage());
    }
}

void MainWindow::save()
{
    QImage p_imgFull = m_view->resultImage();

    QFileDialog p_dlgSave(nullptr, tr("Save Image"), "./",
                          tr("Images (*.jpg)"));
    p_dlgSave.setAcceptMode(QFileDialog::AcceptSave);

    QStringList p_listFiles;

    if ( p_dlgSave.exec() )
        p_listFiles = p_dlgSave.selectedFiles();

    if ( false == p_listFiles.isEmpty() )
    {
        p_imgFull.save(p_listFiles.first());
        m_view->setSavedState(true);
    }
}

void MainWindow::apply()
{
    int p_val = m_spinCore->value();

    if ( p_val % 2 != 1 )
        p_val += 1;

    m_view->setState(new ProcessState(p_val));
}

void MainWindow::initActions()
{
    QMenu *p_menuFile = ui->m_menuMain->addMenu(tr("File"));

    QAction *p_actOpen = new QAction(tr("&Open"), this);
    connect(p_actOpen, SIGNAL(triggered(bool)), this, SLOT(open()));
    p_menuFile->addAction(p_actOpen);

    QAction *p_actApply = new QAction(tr("&Apply"), this);
    connect(p_actApply, SIGNAL(triggered(bool)), this, SLOT(apply()));
    p_menuFile->addAction(p_actApply);

    QAction *p_actSave = new QAction(tr("&Save"), this);
    connect(p_actSave, SIGNAL(triggered(bool)), this, SLOT(save()));
    p_menuFile->addAction(p_actSave);

    QAction *p_actExit = new QAction(tr("&Exit"), this);
    connect(p_actExit, SIGNAL(triggered(bool)), this, SLOT(deleteLater()));
    p_menuFile->addAction(p_actExit);

    m_spinCore = new QSpinBox(this);
    ui->mainToolBar->addWidget(m_spinCore);
}
