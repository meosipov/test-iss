#pragma once

#include <QPointer>
#include <QSpinBox>
#include <QMainWindow>

#include <imageview.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void initActions();

private slots:
    void open();
    void save();
    void apply();

private:
    Ui::MainWindow *ui;

    QPointer <ImageView> m_view;
    QPointer <QSpinBox> m_spinCore;

}; /// class MainWindow
