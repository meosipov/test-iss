#pragma once

#include <QObject>
#include <QScopedPointer>

class QRect;
class QPoint;
class QAction;
class QPainter;
class ImageView;

class ImageStatePrivate;
class ImageState : public QObject
{
    Q_OBJECT

    friend class ImageView;

public:
    /**
     * @brief ImageState
     */
    ImageState();
    ~ImageState();

    /**
     * @brief paint
     * @param _painter
     */
    virtual void paint(QPainter *_painter);

    /**
     * @brief view
     * @return
     */
    ImageView *view() const;

    /**
     * @brief mapToScreen
     * @param _rect
     * @return
     */
    QRect mapToScreen(const QRect &_rect) const;

    /**
     * @brief mapToScreen
     * @param _pos
     * @return
     */
    QPoint mapToScreen(const QPoint &_pos) const;

    /**
     * @brief mapToImage
     * @param _pos
     * @return
     */
    QPoint mapToImage(const QPoint &_pos) const;

    /**
     * @brief mapToImage
     * @param _rect
     * @return
     */
    QRect mapToImage(const QRect &_rect) const;

    /**
     * @brief open
     */
    void open();

protected:
    void setView(ImageView *_view);

protected slots:
    virtual void cleanBuffer();
    void updateBuffer();

    virtual void mouseMove(const QPoint &) {}
    virtual void mousePress(const QPoint &) {}
    virtual void mouseRelease(const QPoint &) {}

private:
    QScopedPointer <ImageStatePrivate> d_ptr;
    Q_DECLARE_PRIVATE(ImageState)

}; /// class ImageState
