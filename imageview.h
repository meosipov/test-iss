#pragma once

#include <QWidget>
#include <QScopedPointer>

class QResizeEvent;
class QMouseEvent;
class QPaintEvent;
class ImageState;

class ImageViewPrivate;

/**
 * @brief The ImageView class
 */
class ImageView : public QWidget
{
    Q_OBJECT

signals:
    void sgnSizeChanged(const QSize &_newSize);
    void sgnStateChanged(ImageState *_state);

public:
    /**
     * @brief ImageView
     * @param parent
     */
    explicit ImageView(QWidget *parent = 0);
    ~ImageView();

    /**
     * @brief setState
     * @param _state
     */
    void setState(ImageState *_state);

    /**
     * @brief setImage
     * @param _image
     */
    void setImage(const QImage &_image);

    /**
     * @brief image
     * @return
     */
    QImage image() const;

    /**
     * @brief setSelection
     * @param _rect
     */
    void setSelection(const QRect &_rect);

    /**
     * @brief selection
     * @return
     */
    QRect selection() const;

    /**
     * @brief setFiltered
     * @param _image
     */
    void setFiltered(const QImage &_image);

    /**
     * @brief filtered
     * @return
     */
    QImage filtered() const;

    /**
     * @brief setSavedState
     * @param _state
     */
    void setSavedState(bool _state);

    /**
     * @brief needSave
     * @return
     */
    bool needSave() const;

    /**
     * @brief resultImage
     * @return
     */
    QImage resultImage() const;

protected:
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    QScopedPointer <ImageViewPrivate> d_ptr;
    Q_DECLARE_PRIVATE(ImageView)

}; /// class ImageView
