#include "imageview.h"

#include <QResizeEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QDebug>
#include <QImage>
#include <QRect>

#include "imagestate.h"

class ImageViewPrivate
{
public:
    ImageViewPrivate()
        :
    m_saved(false)
    {}

    ~ImageViewPrivate()
    {}

    QImage m_imgSource;
    QImage m_imgFiltered;
    QRect m_rectSelect;

    bool m_saved;

    QScopedPointer <ImageState> m_state;

}; /// class ImageViewPrivate

ImageView::ImageView(QWidget *parent)
    : QWidget(parent),
      d_ptr(new ImageViewPrivate)
{
    setMouseTracking(true);
}

ImageView::~ImageView()
{
}

void ImageView::setImage(const QImage &_image)
{
    Q_D(ImageView);

    d->m_imgSource = _image;

    /// TODO: Check state (?)
}

void ImageView::setState(ImageState *_state)
{
    Q_ASSERT(_state);

    Q_D(ImageView);

    _state->setView(this);
    d->m_state.reset(_state);

    update();

    emit sgnStateChanged(_state);
}

QImage ImageView::image() const
{
    Q_D(const ImageView);

    return d->m_imgSource;
}

void ImageView::setSelection(const QRect &_rect)
{
    Q_D(ImageView);

    d->m_rectSelect = _rect;
}

QRect ImageView::selection() const
{
    Q_D(const ImageView);

    return d->m_rectSelect;
}

void ImageView::setFiltered(const QImage &_image)
{
    Q_D(ImageView);

    qDebug() << "Setting filetered image: " << _image.size();
    d->m_imgFiltered = _image;
}

QImage ImageView::filtered() const
{
    Q_D(const ImageView);

    return d->m_imgFiltered;
}

void ImageView::setSavedState(bool _state)
{
    Q_D(ImageView);

    d->m_saved = _state;
}

bool ImageView::needSave() const
{
    Q_D(const ImageView);

    if ( d->m_saved )
        return false;

    if ( false == d->m_imgFiltered.isNull() )
        return true;

    return false;
}

QImage ImageView::resultImage() const
{
    Q_D(const ImageView);

    QImage p_imgRes = d->m_imgSource;

    QPainter p(&p_imgRes);
    p.drawImage(d->m_rectSelect, d->m_imgFiltered);
    p.end();

    return p_imgRes;
}

// cppcheck-suppress unusedFunction
void ImageView::paintEvent(QPaintEvent *event)
{
    Q_D(ImageView);

    QPainter p(this);

    p.fillRect(this->rect(), Qt::green);

    if ( true == d->m_state.isNull() )
    {
        event->accept();
        return;
    }

    d->m_state->paint(&p);

    event->accept();
}

// cppcheck-suppress unusedFunction
void ImageView::resizeEvent(QResizeEvent *event)
{
    emit sgnSizeChanged(event->size());
}

// cppcheck-suppress unusedFunction
void ImageView::mouseMoveEvent(QMouseEvent *event)
{
    Q_D(ImageView);

    if (d->m_state)
        d->m_state->mouseMove(event->pos());
}

// cppcheck-suppress unusedFunction
void ImageView::mousePressEvent(QMouseEvent *event)
{
    Q_D(ImageView);

    if (d->m_state)
        d->m_state->mousePress(event->pos());
}

// cppcheck-suppress unusedFunction
void ImageView::mouseReleaseEvent(QMouseEvent *event)
{
    Q_D(ImageView);

    if (d->m_state)
        d->m_state->mouseRelease(event->pos());
}

